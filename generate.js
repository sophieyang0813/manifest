const {relative} = require('path')
const sherlock = require('@commonshost/sherlock')

const PRIORITY_HIGH = 256
const PRIORITY_MEDIUM = 128
const PRIORITY_LOW = 1
const PRIORITY_DEFAULT = 16

function getPriority (type) {
  switch (type) {
    case 'application/javascript':
    case 'text/css':
      return PRIORITY_HIGH
    case 'font/*':
      return PRIORITY_MEDIUM
    case 'image/*':
      return PRIORITY_LOW
    default:
      return PRIORITY_DEFAULT
  }
}

function normalise (dependencies) {
  const parents = new Map()
  for (const dependency of dependencies) {
    if (!parents.has(dependency.parent)) {
      parents.set(dependency.parent, new Map())
    }
    const priority = getPriority(dependency.type)
    const prioritised = parents.get(dependency.parent)
    if (!prioritised.has(priority)) {
      prioritised.set(priority, new Set())
    }
    prioritised.get(priority).add(dependency.path)
  }
  return parents
}

module.exports.generate =
async function generate (root) {
  const parents = normalise(await sherlock(root))
  const manifest = []
  for (const [parent, prioritised] of parents) {
    const entry = `/${relative(root, parent)}`
    const rule = {get: entry, push: []}
    for (const [priority, dependencies] of prioritised) {
      const glob = Array.from(dependencies)
        .map((path) => `/${relative(root, path)}`)
      const push = {priority, glob}
      rule.push.push(push) // 🌬
    }
    manifest.push(rule)
  }
  return manifest
}
