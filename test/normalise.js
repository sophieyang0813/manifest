const test = require('blue-tape')
const {normalise, validate} = require('..')

const fixtures = [
  {
    scenario: 'Empty manifest',
    given: [],
    expected: []
  },
  {
    scenario: 'Undefined manifest',
    given: undefined,
    expected: []
  },
  {
    scenario: 'Drop pointless rules',
    given: [
      {},
      {uri: 'foo', glob: '/bar'},
      {push: '/foo'}
    ],
    expected: []
  },
  {
    scenario: 'Single rule uri as string',
    given: [{
      uri: '/',
      push: [{glob: ['/foo'], priority: 16}]
    }],
    expected: [{
      get: [{uri: ['/']}],
      push: [{glob: ['/foo'], priority: 16}]
    }]
  },
  {
    scenario: 'Single rule glob as string',
    given: [{
      glob: '**/*',
      push: [{glob: ['/foo'], priority: 16}]
    }],
    expected: [{
      get: [{glob: ['**/*']}],
      push: [{glob: ['/foo'], priority: 16}]
    }]
  },
  {
    scenario: 'Single push as glob string',
    given: [{
      glob: ['**/*'],
      push: '/foo'
    }],
    expected: [{
      get: [{glob: ['**/*']}],
      push: [{glob: ['/foo'], priority: 16}]
    }]
  },
  {
    scenario: 'Single push as uri string',
    given: [{
      glob: ['**/*'],
      push: 'https://example.net/'
    }],
    expected: [{
      get: [{glob: ['**/*']}],
      push: [{uri: ['https://example.net/'], priority: 16}]
    }]
  },
  {
    scenario: 'Single push as object with glob string',
    given: [{
      glob: ['**/*'],
      push: {glob: '/foo'}
    }],
    expected: [{
      get: [{glob: ['**/*']}],
      push: [{glob: ['/foo'], priority: 16}]
    }]
  },
  {
    scenario: 'Single push as object with uri string',
    given: [{
      glob: ['**/*'],
      push: {uri: 'https://example.net/'}
    }],
    expected: [{
      get: [{glob: ['**/*']}],
      push: [{uri: ['https://example.net/'], priority: 16}]
    }]
  },
  {
    scenario: 'Multiple pushes as mixed glob and uri strings',
    given: [{
      glob: ['**/*'],
      push: ['https://example.net/', '**/*']
    }],
    expected: [{
      get: [{glob: ['**/*']}],
      push: [
        {uri: ['https://example.net/'], priority: 16},
        {glob: ['**/*'], priority: 16}
      ]
    }]
  },
  {
    scenario: 'Single push uri as string, default priority',
    given: [{
      glob: ['**/*'],
      push: [{uri: 'https://example.net/'}]
    }],
    expected: [{
      get: [{glob: ['**/*']}],
      push: [
        {uri: ['https://example.net/'], priority: 16}
      ]
    }]
  },
  {
    scenario: 'Custom priority',
    given: [{
      uri: ['/foo', '/bar'],
      glob: ['/foo/**/*.js', '/bar/**/*.css'],
      push: [{priority: 256, glob: '/foo.bar'}]
    }],
    expected: [{
      get: [
        {uri: ['/foo', '/bar']},
        {glob: ['/foo/**/*.js', '/bar/**/*.css']}
      ],
      push: [{priority: 256, glob: ['/foo.bar']}]
    }]
  },
  {
    scenario: 'Wrap push uri & glob in array',
    given: [{
      glob: ['/foo'],
      push: [{
        uri: 'https://example.net/foo.js',
        glob: '/foo'
      }]
    }],
    expected: [{
      get: [{glob: ['/foo']}],
      push: [{
        uri: ['https://example.net/foo.js'],
        glob: ['/foo'],
        priority: 16
      }]
    }]
  },
  {
    // Note: This is convenient when globbing with negatives.
    // It is, however, inconsistent with the default "every array item is a
    // push object" behaviour. Can be reconsidered if it proves unhelpful.
    scenario: 'Multiple push glob strings should be combined',
    given: [{
      glob: ['/foo'],
      // The canonical example is to push everything BUT the source maps.
      push: ['**/*', '!**/*.map']
    }],
    expected: [{
      get: [{glob: ['/foo']}],
      push: [{
        glob: ['**/*', '!**/*.map'],
        priority: 16
      }]
    }]
  },
  {
    scenario: 'Get as a single glob string',
    given: [{
      get: '**/*',
      push: '/foo'
    }],
    expected: [{
      get: [
        {glob: ['**/*']}
      ],
      push: [{priority: 16, glob: ['/foo']}]
    }]
  },
  {
    scenario: 'Get as a single uri string',
    given: [{
      get: 'https://example.net/',
      push: '/foo'
    }],
    expected: [{
      get: [
        {uri: ['https://example.net/']}
      ],
      push: [{priority: 16, glob: ['/foo']}]
    }]
  },
  {
    scenario: 'Get as an array of glob strings',
    given: [{
      get: ['**/*', '!**/*'],
      push: '/foo'
    }],
    expected: [{
      get: [
        {glob: ['**/*', '!**/*']}
      ],
      push: [{priority: 16, glob: ['/foo']}]
    }]
  },
  {
    scenario: 'Get as an array of uri strings',
    given: [{
      get: ['https://example.net/', 'https://example.org/'],
      push: '/foo'
    }],
    expected: [{
      get: [
        {uri: ['https://example.net/']}, {uri: ['https://example.org/']}
      ],
      push: [{priority: 16, glob: ['/foo']}]
    }]
  },
  {
    scenario: 'Shorthand push as array',
    given: [{
      get: '/index.html',
      push: [['/foo', '/bar']]
    }],
    expected: [{
      get: [{glob: ['/index.html']}],
      push: [{
        priority: 16,
        glob: ['/foo', '/bar']
      }]
    }]
  },
  {
    scenario: 'Shorthand push as array',
    given: [{
      get: '/index.html',
      push: [
        '**/*.css',
        {glob: '**/*.js'},
        ['/foo', '/bar'],
        ['https://example.net/', 'https://example.org/'],
        ['/foo', 'https://example.net/', '/bar', 'https://example.org/']
      ]
    }],
    expected: [{
      get: [{glob: ['/index.html']}],
      push: [
        {priority: 16, glob: ['**/*.css']},
        {priority: 16, glob: ['**/*.js']},
        {priority: 16, glob: ['/foo', '/bar']},
        {
          priority: 16,
          uri: ['https://example.net/', 'https://example.org/']
        },
        {
          priority: 16,
          uri: ['https://example.net/', 'https://example.org/'],
          glob: ['/foo', '/bar']
        }
      ]
    }]
  }
]

for (const {scenario, given, expected} of fixtures) {
  test(scenario, async (t) => {
    const actual = normalise(given)
    t.deepEqual(actual, expected)
    t.doesNotThrow(() => validate(actual))
  })
}
